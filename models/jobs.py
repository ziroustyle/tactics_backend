class Jobs:
    id: int = None
    name: str = None
    img: str = None

    def __init__(self, id: int, name: str, img: str = ''):
        self.id = id
        self.name = name
        self.img = img


# HUMES


soldier = Jobs(1, 'Soldier', 'thief.png')
fighter = Jobs(2, 'Fighter')
paladin = Jobs(3, 'Paladin')
thief = Jobs(4, 'Thief')
archer = Jobs(5, 'Archer')
parivir = Jobs(6, 'Parivir')
ninja = Jobs(7, 'Ninja')
hunter = Jobs(8, 'Hunter')
white_mage = Jobs(9, 'White Mage')
black_mage = Jobs(10, 'Black Mage')
blue_mage = Jobs(11, 'Blue Mage')
seer = Jobs(12, 'Seer')
illusionist = Jobs(13, 'Illusionist')


# BANGAA


warrior = Jobs(14, 'Hunter')
dragoon = Jobs(15, 'Dragoon')
gladiator = Jobs(16, 'Gladiator')
cannoneer = Jobs(17, 'Cannoneer')
defender = Jobs(18, 'Defender')
white_monk = Jobs(19, 'White Monk')
master_monk = Jobs(20, 'Master Monk')
bishop = Jobs(21, 'Bishop')
trickster = Jobs(22, 'Trickster')
templar = Jobs(23, 'Templar')


# NU MOU


time_mage = Jobs(24, 'Time Mage')
arcanist = Jobs(25, 'Arcanist')
beastmaster = Jobs(26, 'Beast Master')
sage = Jobs(27, 'Sage')
scholar = Jobs(28, 'Scholar')
alchemist = Jobs(29, 'Alchemist')


# VIERA


fencer = Jobs(30, 'Fender')
red_mage = Jobs(31, 'Red Mage')
spell_blade = Jobs(32, 'Spell Blade')
green_mage = Jobs(33, 'Green Mage')
elementalist = Jobs(34, 'Elementalist')
summoner = Jobs(35, 'Summoner')
sniper = Jobs(36, 'Sniper')
assassin = Jobs(37, 'Assassin')


# MOOGLE


animist = Jobs(38, 'Animist')
moogle_knight = Jobs(39, 'Moogle Knight')
fusilier = Jobs(40, 'Fusilier')
chocobo_knight = Jobs(41, 'Chochobo Knight')
flintlock = Jobs(42, 'Flintlock')
juggler = Jobs(43, 'Juggler')
tinker = Jobs(44, 'Tinker')
