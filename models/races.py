from models.jobs import *


class Hume:
    id: int = None
    name: str = None
    jobs: Jobs = []

    def __init__(self):
        self.id = 1
        self.name = 'Hume'
        self.jobs = [
            soldier,
            fighter,
            paladin,
            thief,
            archer,
            parivir,
            ninja,
            hunter,
            white_mage,
            black_mage,
            blue_mage,
            seer,
            illusionist
         ]


class Bangaa:
    id: int = None
    name: str = None
    jobs: Jobs = []

    def __init__(self):
        self.id = 2
        self.name = 'Bangaa'
        self.jobs = [
            warrior,
            dragoon,
            gladiator,
            cannoneer,
            defender,
            white_monk,
            master_monk,
            bishop,
            trickster,
            templar
        ]


class Numou:
    id: int = None
    name: str = None
    jobs: Jobs = []

    def __init__(self):
        self.id = 3
        self.name = 'Nu mou'
        self.jobs = [
            black_mage,
            time_mage,
            arcanist,
            beastmaster,
            sage,
            scholar,
            white_mage,
            illusionist,
            alchemist
        ]


class Viera:
    id: int = None
    name: str = None
    jobs: Jobs = []

    def __init__(self):
        self.id = 4
        self.name = 'Viera'
        self.jobs = [
            white_mage,
            archer,
            fencer,
            red_mage,
            spell_blade,
            green_mage,
            elementalist,
            summoner,
            sniper,
            assassin
        ]


class Moogle:
    id: int = None
    name: str = None
    jobs: Jobs = []

    def __init__(self):
        self.id = 5
        self.name = 'Moogle'
        self.jobs = [
            animist,
            moogle_knight,
            fusilier,
            chocobo_knight,
            flintlock,
            juggler,
            tinker,
            thief,
            black_mage,
            time_mage
        ]
