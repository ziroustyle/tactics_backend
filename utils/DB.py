from tinydb import TinyDB, Query
import uuid
import secrets

db = TinyDB('db.json')
USERS_TABLE = 'users'
PLAYERS_TABLE = 'players'


def reset_users(db: TinyDB):
    db.purge_table(USERS_TABLE)
    users_table = db.table(USERS_TABLE)
    users_table.insert({
        'id': str(uuid.uuid4()),
        'name': 'admin',
        'password': 'admin',
        'first_time': True,
        'token': secrets.token_hex(20)
    })

    db.purge_table(PLAYERS_TABLE)
