from aiohttp import web
import aiohttp_cors
import secrets
import uuid

from models.races import *
from utils.utils import *
from utils.DB import *


async def default(request):
    return web.json_response({'response': 'no existe metodo'})


async def login(request):
    data = await request.json()
    username = data['user']
    password = data['password']

    user_table = db.table(USERS_TABLE)
    user = Query()
    result = user_table.search(user.name == username)

    if len(result) > 0:
        if username == result[0]['name'] and password == result[0]['password']:
            # return web.json_response({'token': secrets.token_hex(20)})
            return web.json_response({'token': result[0]['token'], 'id': result[0]['id']})
    else:
        return web.Response(status=401, text='Usuario o password invalidos')


async def signup(request):
    data = await request.json()

    username = data['user']
    password = data['password']
    repassword = data['repassword']

    user_table = db.table(USERS_TABLE)
    user = Query()
    result = user_table.search(user.name == username)

    if username == '' or password == '' or repassword == '':
        return web.Response(status=401, text='Datos incorrectos')
    elif password != repassword:
        return web.Response(status=401, text='Debes repetir el password 2 veces')
    elif len(result):
        return web.Response(status=401, text='El nombre de usuario ya existe')
    else:
        users_table = db.table(USERS_TABLE)
        id = str(uuid.uuid4())
        users_table.insert({
                            'id': id,
                            'name': username,
                            'password': password,
                            'token': secrets.token_hex(20),
                            'first_time': True})
        players_table = db.table(PLAYERS_TABLE)
        players_table.insert({
                            'id': id,
                            'nickname': None,
                            'race': None,
                            'job': None,
                            'Equip': []
                            })

        return web.json_response(status=200)


async def get_current_player(request):
    data = await request.json()
    id = data['id']

    users_table = db.table(USERS_TABLE)
    user = Query()
    result_user = users_table.search(user.id == id)

    players_table = db.table(PLAYERS_TABLE)
    player = Query()
    result_player = players_table.search(player.id == result_user[0]['id'])

    return web.json_response(result_player)


async def new_hero(request):
    data = await request.json()
    id = data['id']
    nickname = data['nickname']
    race = data['race']
    job = data['job']

    players_table = db.table(PLAYERS_TABLE)
    Player = Query()
    players_table.update({'nickname': nickname, 'race': race['id'], 'job': job['id']}, Player.id == id)

    return web.json_response(status=200)


async def get_all_races(request):
    races = [
        Hume(),
        Bangaa(),
        Numou(),
        Viera(),
        Moogle()
    ]

    result = MyEncoder().encode(races)

    return web.json_response(result)


# reset_users(db)
app = web.Application()
app.router.add_get('/', default)
cors = aiohttp_cors.setup(app, defaults={
    "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
})
cors.add(app.router.add_post('/login', login))
cors.add(app.router.add_post('/signup', signup))
cors.add(app.router.add_post('/get_current_player', get_current_player))
cors.add(app.router.add_post('/new_hero', new_hero))
cors.add(app.router.add_get('/get_all_races', get_all_races))
web.run_app(app)
